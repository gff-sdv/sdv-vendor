# WP SDV Vendor

The `SDV Vendor` plugin.

## New release

1. Set the new version **X.Y** in the `sdv-vendor.php` file comments section.
2. Add a new repository tag **X.Y** without writing release notes.
3. Gitlab CI would create a release asset.
4. Once finished (see pipeline), update the stable version in `readme.txt` and commit/push.
