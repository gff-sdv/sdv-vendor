<?php

/*
Plugin Name: SDV Vendor
Description: Provides a lot of third-party libraries used by many sdv plugins.
Version: 1.1
Author: GFF Integrative Kommunikation GmbH
Author URI: https://www.gff.ch/
License: Proprietary
*/

declare( strict_types=1 );

include_once __DIR__ . '/vendor/autoload.php';

call_user_func( function () {
	$updateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/gff-sdv/sdv-vendor/',
		__FILE__,
		'sdv-vendor'
	);

	/** @var Puc_v4p11_Vcs_GitLabApi $vcsApi */
	$vcsApi = $updateChecker->getVcsApi();
	$vcsApi->enableReleasePackages();
} );

add_action( 'plugins_loaded', function () {
	load_plugin_textdomain( 'sdv-vendor', false, 'sdv-vendor/locales' );
} );
