��          <      \       p   A   q   
   �      �   f  �   p   9  
   �     �                   Provides a lot of third-party libraries used by many sdv plugins. SDV Vendor https://www.gff.ch/ Project-Id-Version: SDV Vendor 0.7
Report-Msgid-Bugs-To: adrian.suter@gff.ch
PO-Revision-Date: 2022-06-20 17:31+0200
Last-Translator: Adrian Suter <adrian.suter@gff.ch>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1
 GFF SDV Vendor stellt Drittanbieter-Bibliotheken zur Verfügung, welche von vielen SDV Plugins benötigt werden. SDV Vendor https://www.gff.ch/ 