<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'e8a0d63352f45ecdef42ae52676f683285b0d9d4',
        'name' => 'gff/sdv-vendor',
        'dev' => true,
    ),
    'versions' => array(
        'gff/sdv-vendor' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'e8a0d63352f45ecdef42ae52676f683285b0d9d4',
            'dev_requirement' => false,
        ),
        'yahnis-elsts/plugin-update-checker' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../yahnis-elsts/plugin-update-checker',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => '8d78365380aa04e9e2601cdf21b8e3c45127b44b',
            'dev_requirement' => false,
        ),
    ),
);
