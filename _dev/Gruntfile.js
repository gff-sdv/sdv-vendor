module.exports = function (grunt) {
  // noinspection JSUnresolvedFunction
  grunt.initConfig({
    makepot: {
      main: {
        options: {
          cwd: '../',
          mainFile: 'sdv-vendor.php',
          domainPath: 'locales',
          potHeaders: {
            'Report-Msgid-Bugs-To': 'adrian.suter@gff.ch'
          },
          exclude: ['_dev', 'vendor'],
          processPot: function (pot) {
            delete pot['translations']['']['https://www.gff.ch'];
            delete pot['translations']['']['GFF Integrative Kommunikation GmbH'];
            delete pot['translations']['']['Settings'];

            return pot;
          }
        }
      }
    }
    ,
    compress: {
      main: {
        options: {
          archive: function () {
            return '../sdv-vendor.zip';
          },
          mode: 'zip'
        },
        files: [
          {
            expand: true,
            cwd: '../',
            src: [
              '**',
              '!_dev/**',
              '!assets/*.xcf',
              '!.gitignore',
              '!.gitlab-ci.yml',
              '!composer.json',
              '!composer.lock',
              '!README.md',
            ],
            dest: 'sdv-vendor/'
          }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-wp-i18n');
  grunt.loadNpmTasks('grunt-contrib-compress');

  // Register the tasks.
  grunt.registerTask('archive', ['compress']);
};
