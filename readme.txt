=== SDV Vendor ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.0
Tested up to: 6.7
Requires PHP: 7.4
Stable tag: 1.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Provides GFF SDV third-party libraries.

== Description ==

This plugin provides GFF SDV third-party libraries used by other GFF SDV plugins.

== Frequently Asked Questions ==

None.

== Upgrade Notice ==

Please upgrade as soon as possible.

== Changelog ==

= 1.1 =
* WordPress compatibility.

= 1.0 =
* First official version.
